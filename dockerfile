# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY target/member-buymileage-buy.jar /member-buymileage-buy.jar
# run application with this command line[
CMD ["java", "-jar", "/member-buymileage-buy.jar"]
